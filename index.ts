const Telegraf = require("telegraf");
import { NewInvoiceParameters } from "telegraf/typings/telegram-types";
import { TelegrafContext } from "telegraf/typings/context";

import { env } from "./env/env";
import strings from "./strings";

const bot = new Telegraf(env.token);

bot.start((message: TelegrafContext) => {
    message.reply(`${strings.welcomeMessage}`);
    message.reply(`${strings.howMuch}`);
});

bot.on("text", (message: TelegrafContext) => {
    const text = parseInt(message.message.text);
    isNaN(text) ? message.reply(`${strings.notANumber}`) : null;
    
    const invoice: NewInvoiceParameters = {
        title: `Donation`,
        description: `Donation of £${text}.00`,
        start_parameter: `donation_${text}`,
        currency: "gbp",
        provider_token: env.stripeToken,
        prices: [
            {label: "Amount", amount: parseInt(`${text}00`)}
        ],
        payload: `donation_${text}`
    }

    message.replyWithInvoice(invoice);

}); 

bot.on("pre_checkout_query", (message: TelegrafContext) => {
    message.answerPreCheckoutQuery(true)
});

bot.on("successful_payment", (message: TelegrafContext) => {
    message.replyWithSticker(`CAACAgIAAxkBAAMkXqTf6-Z_9oVuhCkh8yhTVJY79wADFQADwDZPE81WpjthnmTnGQQ`);
    message.reply(`${strings.paymentSuccess}`);
});

bot.command("support", (message: TelegrafContext) => {
    message.reply(`${strings.support}`)
})

bot.launch()