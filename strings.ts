const strings = {
    welcomeMessage: `👋 Hi`,
    howMuch: `How much do you want to donate? (as an example 31 or 9)`,
    notANumber: `Hmmm.... i need a number, please try again`,
    havingTrouble: `Im having trouble here, could you please try sending that again?`,
    paymentSuccess: `You're the best! thanks so much for your kind donation`,
    support: `We dont handle any information, all data is stored by telegram or stripe please reach out to their support teams with any questions`
}

export default strings;