# Telegram Payment Bot

### Set up telegram bot
open the telegram [bot father](https://t.me/botfather) to create a new bot, follow the instructions to set up a new bot   
select the command `mybots` from the results select the bot you have created, select the option `payments` choose a payment processor and follow the steps to connect telegram with your payment processor

### Installation
clone the repository 
```bash
git@bitbucket.org:mogold29/telegram-payment-bot.git
```
Within the folder `env` rename the file `env.template.ts` to `env.ts` paste in the tokens from telegram bot father, the bot token and the payment token  
 
install modules from npm
```bash
npm i
```

and run the bot
```bash
npm run build
```


